import { useSsr } from 'usehooks-ts'
import { store as apiStore } from './apistore.js'
import { useContext} from 'react'

const fetcher = async (url) => {
    return fetch(url).then( (res) => {
      const json = res.json()
      return json ? json : undefined
    }).catch( err => {
      if(!err.message.localeCompare('NetworkError when attempting to fetch resource.')) {
        const error = new Error('Connection error')
        throw error
      } else {
        throw err
      }
    })
  };

  
const APIWrap = (useSWRNext) => {
    return (key, fetcher, config) => {
        const { isBrowser } = useSsr()

        const apiState = useContext(apiStore)
        const { dispatch } = apiState

        const startTime = new Date().getTime();
        
        if(isBrowser) {
          console.log('SWR Request:', key)
          dispatch( {type: 'setRoute', payload: key} )
          dispatch( {type: 'setIsLoading', payload: true} )
          dispatch( {type: 'setElapsed', payload: 0} )
        }
    
        // get swr from original fetcher
        const swr = useSWRNext(key, fetcher, config)
        const { data, error } = swr
        if(data || error) {
          dispatch( {type: 'setIsLoading', payload: false} )
          dispatch( {type: 'setElapsed', payload: new Date().getTime() - startTime.current} )
        }

        if(error) {
          dispatch( {type: 'setError', payload: error} )
        } 
      
        return swr
    }
  }
  
  export { fetcher, APIWrap }