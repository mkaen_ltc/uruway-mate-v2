import React, { createContext, useReducer } from 'react';

// api state
const initialState = { error: undefined, isLoading: false };
const store = createContext(initialState);
const { Provider } = store;


const StateProvider = ({ children }) => {
    const [state, dispatch] = useReducer((state, action) => {
        switch (action.type) {
            case 'setRoute':
                state.route = action.payload
                return state
            case 'setError':
                state.error = action.payload
                return state
            case 'setIsLoading':
                state.isLoading = action.payload
                return state
            case 'setElapsed':
                state.elapsed = action.payload
                return state
            default:
                throw new Error();
        };
    }, initialState);

    return <Provider value={{ state, dispatch }}>{children}</Provider>;
};

export { store, StateProvider }