import React, { createContext, useReducer } from 'react';

// global state
const initialState = { lang: 'es', error: undefined, user: {} };
const store = createContext(initialState);
const { Provider } = store;

const StateProvider = ({ children }) => {
    const [state, dispatch] = useReducer((state, action) => {
        switch (action.type) {
            case 'setUser':
                state.user = action.payload;
                return state;
            case 'updateUser':
                return { ...state, user: action.payload }
            case 'setError':
                state.error = action.payload
                return state
            default:
                throw new Error();
        };
    }, initialState);

    return <Provider value={{ state, dispatch }}>{children}</Provider>;
};

export { store, StateProvider }