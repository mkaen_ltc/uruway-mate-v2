import React, { useContext } from 'react';

const authContext = React.createContext({missingToken: false, isLoading: false, isError: false, error: '', user: {}});

export default authContext