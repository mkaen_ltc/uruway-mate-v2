import useSWR from "swr"

//const fetcher = url => fetch(url).then(res => res.json())

const useGetTeams = path => {
    if (!path) {
        throw new Error("Path is required")
    }
    const { data: teams, error } = useSWR(path)
    return { teams, error }
}

const useGetTeam = (path) => {
    if (!path) {
        throw new Error("Path is required")
    }
    const { data: team } = useSWR(path)
    return team;
}

const useGetTeamMembers = (path) => {
    if (!path) {
        throw new Error("Path is required")
    }
    const { data: membersList } = useSWR(path)
    return membersList;
}

export { useGetTeams, useGetTeam, useGetTeamMembers }