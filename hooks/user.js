import useSWR from "swr";
import { useLayoutEffect, useEffect } from "react";
import Router from "next/router";

export function useAuthSession() {
  const { data: user, error } = useSWR("api/users/me");
  const isLoading = (!user && !error)
  console.log(" user " + user)
  console.log(" is loading " + isLoading)
  useEffect(() => {
    if (user?.missingToken) {
        Router.push("/");
        return null
    }
    if (!user && !isLoading) Router.push("/");
  }, [user]);
  return user;
}

