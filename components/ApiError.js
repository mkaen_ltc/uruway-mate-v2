import React from "react";
import { useStoreState } from "easy-peasy";
import Snackbar from "@mui/material/Snackbar";
import Grow from "@mui/material/Grow";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";

const ApiError = ({ props }) => {
  const error = useStoreState((state) => state.error);
  const isError = useStoreState((state) => state.isError);
  const [open, setOpen] = React.useState(isError);

  const action = (
    <React.Fragment>
      <IconButton
        size="small"
        aria-label="close"
        color="inherit"
        onClick={() => setOpen(false)}
      >
        <CloseIcon fontSize="small" />
      </IconButton>
    </React.Fragment>
  );

  return (
    <>
      <Snackbar
        open={open}
        TransitionComponent={Grow}
        action={action}
        message={error?.message}
      ></Snackbar>
    </>
  );
};

export default ApiError;