import * as React from 'react';
import MenuItem from '@mui/material/MenuItem';
import TranslateIcon from '@mui/icons-material/Translate';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import { i18n } from '@lingui/core';
import styles from './select.module.css';

/* Componente https://mui.com/components/selects/ */

export default function LangSelector() {
    const handleChange = (event) => {
        i18n.activate(event.target.value);
    };
    return (
        <>
        <TranslateIcon sx={{ margin: '.2em' }} />
        <FormControl fullWidth variant={'standard'}>
            <Select
                labelId="lang-selector"
                id="lang-selector"
                value={i18n.locale}
                label="Lang"                
                onChange={handleChange}
                className={styles.white}
                //color={theme.palette.white}
            >            
                <MenuItem value='es'>Español</MenuItem>
                <MenuItem value='en'>English</MenuItem>
            </Select>
        </FormControl>
        </>
        )
}