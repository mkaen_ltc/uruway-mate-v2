import {useState, useContext, useEffect} from 'react';
import Router from 'next/router';
import useSWR from 'swr';
import { useSsr } from 'usehooks-ts'

import { store, StateProvider as GlobalStateProvider} from '../src/globalstore.js';


const CheckAuth = ({children}) => {
    const { isBrowser } = useSsr()
    const globalState = useContext(store)
    const { dispatch } = globalState
  
    const [ visible, setVisible] = useState(false)
    const { data: user, error } = useSWR('/api/users/me')

    useEffect(() => {
      if(!error) dispatch({ type: 'setUser', payload: user})
      if(Router.pathname === '/login' && user?.email) { Router.push('/'); return null} // redirect to / if logged in
      if(Router.pathname === '/login') { setVisible(true); return null};
      if(user?.missingToken) { Router.push('/login'); return null} // if data requires token and its not there, goto login
      if(user || error) setVisible(true) // set visible when finished loading on data or error
     }, [user, error, visible])

    if(isBrowser && Router.pathname === '/login') return children
    if(visible) return children
    return null
}

export default CheckAuth