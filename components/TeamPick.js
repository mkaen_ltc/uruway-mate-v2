import React, { useState, useEffect } from 'react';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import whiteSel from './select.module.css';
import { useGetTeams } from "../hooks/useRequest";

export default function CustomizedSelects() {
  const [team, setTeam] = React.useState('');

  // Obtengo el listado de teams
  const { teams, error } = useGetTeams("/api/users/teams");

  // Handle para el cambio de team
  const handleChange = (event) => {
    setTeam(event.target.value);
  };

  // Guardo en state el primer team, luego será el ultimo que usó cuando este el endpoint
  useEffect(() => {
    if (teams) {
      setTeam(Object.values(teams)[0].uuid);
    }
  }, [teams]);

  if (!teams) return <h1>Loading...</h1>
  return (
    <>
      <FormControl variant="standard">
        <Select
          id="demo-customized-select"
          value={team}
          onChange={handleChange}
          className={whiteSel.white}
        >
          {Object.entries(teams).map(team => {
            return (
              <MenuItem key={team[1].uuid} value={team[1].uuid}>{team[1].name}</MenuItem>
            )
          })}
        </Select>
      </FormControl>
    </>
  );
}
