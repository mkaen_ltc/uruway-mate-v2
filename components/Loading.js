import React, { useEffect, useContext } from "react";
import Snackbar from "@mui/material/Snackbar";
import Alert from "@mui/material/Alert";
import Grow from "@mui/material/Grow";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import { store as apiStore } from '../src/apistore.js';

function Loading(props) {
    // use api specific store to avoid unnecesary redraws
    const apiState = useContext(apiStore)
    const { state, dispatch } = apiState

    const [open, setOpen] = React.useState(false);
    const [isClosing, setIsClosing] = React.useState(false);
    const [message, setMessage] = React.useState(state.route ?? false)

    let error = undefined
    if (state.error !== undefined) {
        error = <Alert severity="warning">{state.error?.toString() + "... retrying"}</Alert>
    }

    useEffect(() => {
        state.isLoading && setOpen(true)
        //      setMessage(state.route + " " + state.elapsed + "ms")
        const timer = setTimeout(() => {
            setOpen(false)
        }, 5000)

        return () => { clearTimeout(timer) }
    }, [state.isLoading, state.error])

    const action = (
        <React.Fragment>
            <IconButton
                size="small"
                aria-label="close"
                color="inherit"
                onClick={() => setOpen(false)}
            >
                <CloseIcon fontSize="small" />
            </IconButton>
        </React.Fragment>
    );

    return (
        <>
            <Snackbar
                open={open}
                TransitionComponent={Grow}
                action={action}
                message={state.route + (state.elapsed > 0 ? (' ' + state.elapsed + ' ms') : '')}
            ></Snackbar>
            {error}
        </>
    );
}

export default Loading