import React, { useContext } from 'react';
import { styled, alpha } from '@mui/material/styles';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import InputBase from '@mui/material/InputBase';
import Badge from '@mui/material/Badge';
import MenuItem from '@mui/material/MenuItem';
import Menu from '@mui/material/Menu';
import Typography from '@mui/material/Typography';
//import Link from '@mui/material/Link';
import Link from 'next/link';
import AccountCircle from '@mui/icons-material/AccountCircle';
import SupportIcon from '@mui/icons-material/Support';
import ArticleIcon from '@mui/icons-material/Article';
import ForumIcon from '@mui/icons-material/Forum';
import NotificationsIcon from '@mui/icons-material/Notifications';
import MoreIcon from '@mui/icons-material/MoreVert';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import Gravatar from 'react-gravatar';
import TeamPick from './TeamPick';
import styles from './AppBar.module.css';
import { store as globalStore } from "../src/globalstore.js";
import Router from 'next/router';

import { Trans } from '@lingui/macro';

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: 'inherit',
  '& .MuiInputBase-input': {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '20ch',
    },
  },
}));

export default function PrimarySearchAppBar() {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);

  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  const handleProfileMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
  };

  // Función de Signout
  const handleSignout = async () => {
    const res = await fetch('/api/users/signout');
    const result = await res.json();
    if (result.ok) {
      Router.push('/login');
    }
  };

  const handleMobileMenuOpen = (event) => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const { state } = useContext(globalStore)

  const menuId = 'primary-search-account-menu';
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      id={menuId}
      keepMounted
      transformOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem onClick={handleMenuClose} className={styles.noline}>
        <Trans>
          <Link href="/profile">Profile</Link>
        </Trans>
      </MenuItem>
      <MenuItem onClick={handleMenuClose} className={styles.noline}>
        <Trans>
          <Link href="#">My account</Link>
        </Trans>
      </MenuItem>
      <MenuItem onClick={handleSignout} className={styles.noline}>
        <Trans>
          Signout
        </Trans>
      </MenuItem>
    </Menu>
  );

  const mobileMenuId = 'primary-search-account-menu-mobile';
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem>
        <IconButton size="large" aria-label="soporte" color="inherit">
          <SupportIcon />
        </IconButton>
        <p><Trans>Support</Trans></p>
      </MenuItem>
      <MenuItem>
        <IconButton size="large" aria-label="docs" color="inherit">
          <ArticleIcon />
        </IconButton>
        <p><Trans>Docs</Trans></p>
      </MenuItem>
      <MenuItem>
        <IconButton size="large" aria-label="blog" color="inherit">
          <ForumIcon />
        </IconButton>
        <p>Blog</p>
      </MenuItem>
      <MenuItem>
        <IconButton
          size="large"
          aria-label="show 17 new notifications"
          color="inherit"
        >
          <Badge badgeContent={17} color="error">
            <NotificationsIcon />
          </Badge>
        </IconButton>
        <p><Trans>Notifications</Trans></p>
      </MenuItem>
      <MenuItem onClick={handleProfileMenuOpen}>
        <IconButton
          size="large"
          aria-label="account of current user"
          aria-controls="primary-search-account-menu"
          aria-haspopup="true"
          color="inherit"
        >
          <AccountCircle />
        </IconButton>
        <p><Trans>Profile</Trans></p>
      </MenuItem>
    </Menu>
  );
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <Link href="/">
            <Box
              component="img"
              sx={{
                height: 45,
                width: 45,
              }}
              alt="isologo latin cloud"
              src="/img/iso-ltc.png"
            />
          </Link>
          {state.user &&
            <>
              <Gravatar email={state.user.email} size={30} className={styles.rounded} />
            </>
          }
          <TeamPick></TeamPick>
          <Box sx={{ flexGrow: 1 }} />
          <Box sx={{ display: { xs: 'none', md: 'flex' } }}>
            <List
              sx={{
                display: 'flex',
                flexFlow: 'row wrap',
              }}>
              <ListItemButton>
                <ListItemText primary={<Trans>Docs</Trans>} />
              </ListItemButton>
              <ListItemButton>
                <ListItemText primary="Blog" />
              </ListItemButton>
              <ListItemButton>
                <ListItemText primary={<Trans>Support</Trans>} />
              </ListItemButton>
            </List>
            <IconButton
              size="large"
              aria-label="show 17 new notifications"
              color="inherit"
            >
              <Badge badgeContent={17} color="error">
                <NotificationsIcon />
              </Badge>
            </IconButton>
            <IconButton
              size="large"
              edge="end"
              aria-label="account of current user"
              aria-controls={menuId}
              aria-haspopup="true"
              onClick={handleProfileMenuOpen}
              color="inherit"
            >
              <AccountCircle />
            </IconButton>
          </Box>
          <Box sx={{ display: { xs: 'flex', md: 'none' } }}>
            <IconButton
              size="large"
              aria-label="show more"
              aria-controls={mobileMenuId}
              aria-haspopup="true"
              onClick={handleMobileMenuOpen}
              color="inherit"
            >
              <MoreIcon />
            </IconButton>
          </Box>
        </Toolbar>
      </AppBar>
      {renderMobileMenu}
      {renderMenu}
    </Box>
  );
}
