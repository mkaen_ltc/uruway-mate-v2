import { NextRequest, NextResponse } from 'next/server'

const COOKIE_NAME = 'ltcsess'

// Redirect to login if no session cookie is found
export function middleware(req: NextRequest) {
/*  
  const { pathname } = req.nextUrl
  console.log(req.cookies[COOKIE_NAME])
  console.log(pathname)
  if(pathname == '/') return NextResponse.next()

  if(req.cookies[COOKIE_NAME] === undefined)
    return NextResponse.redirect('/')

*/
  return NextResponse.next()
}