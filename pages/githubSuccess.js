import * as React from 'react';
import Typography from '@mui/material/Typography';
import GitHubIcon from '@mui/icons-material/GitHub';
import Box from '@mui/material/Box';

export default function githubSuccess() {

    return (
        <>
            <Box display="flex" justifyContent="center" alignItems="center" m={2}>
                <Box ml={2}>
                    <GitHubIcon />
                    <Typography component="h2" color="inherit">
                        Successfully logged in with Github
                    </Typography>
                </Box>
            </Box>
        </>
    );
}