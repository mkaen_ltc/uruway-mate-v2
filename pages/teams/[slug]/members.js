import React from 'react';
import { useRouter } from 'next/router'

import { useGetTeam } from "../../../hooks/useRequest";
import TeamMembersList from "../../../components/TeamMembersList";
import TeamMemberInvite from "../../../components/TeamMemberInvite";

export default function members() {
    const router = useRouter();
    const { slug } = router.query;
    const team = useGetTeam("/api/teams/" + slug);

    if (!team) return <h1>Loading...</h1>
    
    return (
        <>
            <TeamMemberInvite team={team} />
            <TeamMembersList team={team} />
        </>
    )
}