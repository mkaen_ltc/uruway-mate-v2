import React, { useState } from 'react';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import LoadingButton from '@mui/lab/LoadingButton';
import { Trans } from '@lingui/macro';
import slugify from 'react-slugify';
import { post } from "../../services/codeapi";
import Router from 'next/router';

export default function create() {
    const [data, setData] = useState({
        name: '',
        slug: ''
    });
    const [loading, setLoading] = useState(false);

    // Handle name input
    const handleNameChange = event => {
        setData({
            ...data,
            ['name']: event.target.value,
            ['slug']: slugify(event.target.value)
        });
    }

    // Handle slug input
    const handleSlugChange = event => {
        setData({
            ...data,
            ['slug']: event.target.value
        });
    }

    const handleCreate = async event => {
        event.preventDefault();
        setLoading(true);
        const response = await post('/teams', { name: data.name, slug: data.slug });
        if(response.ok)
        {
            Router.push('/teams');
        }
        else
        {
            setLoading(false);
        }
    }

    return (<>
        <Grid container spacing={6}>
            <Grid item xs>
                <Box ml={2}>
                    <form onSubmit={handleCreate}>
                        <Typography component="h1" color="inherit">
                            <Trans>Team Create</Trans>
                        </Typography>
                        <div>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                id="name"
                                label={<Trans>Team Name</Trans>}
                                name="name"
                                type="text"
                                autoFocus
                                required
                                value={data.name}
                                onChange={handleNameChange}
                            />
                        </div>
                        <div>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                id="slug"
                                label={<Trans>Slug</Trans>}
                                name="slug"
                                type="text"
                                required
                                value={data.slug}
                                onChange={handleSlugChange}
                            />
                        </div>
                        <LoadingButton
                            type="submit"
                            variant="contained"
                            color="primary"
                            loading={loading}
                            loadingPosition="start"
                        >
                            <Trans>Create</Trans>
                        </LoadingButton>
                    </form>
                </Box>
            </Grid>
        </Grid>
    </>);
}