import * as React from 'react';
import Layout from "../components/Layout";
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';

export default function activity() {

    return (
        <>
            <Box display="flex" justifyContent="center" alignItems="center" m={2}>
                <Box ml={2}>
                    <Typography component="h2" color="inherit">
                        Activity
                    </Typography>
                </Box>
            </Box>
        </>
    );
}